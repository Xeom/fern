use std::ops::Add;

#[derive(Copy, Clone, Debug)]
pub struct Movement
{
    pub x: i64,
    pub y: i64
}

pub trait Movable
{
    fn moved(&self, mov: Movement) -> Self;
}

pub trait InnerMovable
{
    fn do_move(self, mov: Movement);
}

impl Movement
{
    fn new(y: i64, x: i64) -> Self { Self { x: x, y: y } }

    pub fn left (x: i64) -> Self { Self::new( 0, -x) }
    pub fn right(x: i64) -> Self { Self::new( 0,  x) }
    pub fn up   (y: i64) -> Self { Self::new(-y,  0) }
    pub fn down (y: i64) -> Self { Self::new( y,  0) }
}

impl Add for Movement
{
    type Output = Self;

    fn add(self, oth: Self) -> Self {
        Self::new(self.y + oth.y, self.x + oth.x)
    }
}
