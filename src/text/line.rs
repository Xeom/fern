use super::{ToCol};
use std::cmp;

#[derive(Clone, Debug)]
pub struct Line
{
    text: Vec<char>
}

impl Line
{
    pub const fn new() -> Self { Self { text: Vec::new() } }

    pub fn from_chars(chrs: &[char]) -> Self {
        Self { text: chrs.to_vec() }
    }

    pub fn from_string(string: &str) -> Self {
        Self { text: string.chars().collect() }
    }

    pub fn to_ind(&self, pos: &dyn ToCol) -> usize {
        cmp::min(self.len(), cmp::max(0, i64::from(pos.to_col())) as usize)
    }

    pub fn insert_chars(&mut self, pos: &dyn ToCol, chrs: &[char])
    {
        let ind = self.to_ind(pos);
        self.text.splice(ind..ind, chrs.iter().cloned());
    }

    pub fn split(mut self, pos: &dyn ToCol) -> (Line, Line)
    {
        let after = Self { text: self.text.split_off(self.to_ind(pos)) };

        return (self, after);
    }

    pub fn before(self, pos: &dyn ToCol) -> Line { self.split(pos).0 }
    pub fn after (self, pos: &dyn ToCol) -> Line { self.split(pos).1 }


    pub fn append(&mut self, mut line: Line)
    {
        self.text.append(&mut line.text);
    }

    pub fn len(&self) -> usize { self.text.len() }

    pub fn chars<'a>(&'a self) -> impl Iterator<Item=char> + 'a {
        self.text.iter().cloned()
    }
}
