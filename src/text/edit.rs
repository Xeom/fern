use super::{Movement, MutText, Pos, Shift, Line, Movable};
use crate::cursor::{Cursor, CursorText};
use std::cmp::{min, max};

pub trait EditableText: MutText + CursorText
{
    fn break_line_at(&self, pos: Pos)
    {
        if let Some(oldline) = self.get_line(&pos)
        {
            let (before, after) = oldline.split(&pos);

            self.replace_lines(&pos, 1, vec![before, after]);
            self.shift_all(Shift::new(
                pos,
                pos.moved(Movement::down(1)).start_of_line()
            ));
        }
    }

    fn delete_between(&self, p1: Pos, p2: Pos)
    {
        let start = min(p1, p2);
        let end   = max(p1, p2);
        let nrows = Shift::new(start, end).to_movement().y + 1;

        let mut new = Line::new();

        if let Some(line1) = self.get_line(&start)
        {
            new.append(line1.before(&start));
        }

        if let Some(line2) = self.get_line(&end)
        {
            new.append(line2.after(&end));
        }

        self.replace_lines(&start, nrows as usize, vec![new]);
        self.shift_all(Shift::new(end, start));
    }

    fn insert_chars_at(&self, pos: Pos, text: &[char])
    {
        if let Some(mut line) = self.get_line(&pos)
        {
            line.insert_chars(&pos, text);
            self.replace_lines(&pos, 1, vec![line]);
            self.shift_all(
                Shift::from_movement(
                    pos,
                    Movement::right(text.len() as i64)
                )
            )
        }
    }

    fn insert_at(&self, pos: Pos, text: &str)
    {
        let mut inspos: Pos = pos;

        for (ind, line) in text.lines().enumerate()
        {
            if ind != 0
            {
                self.break_line_at(inspos);
                inspos = inspos.moved(Movement::down(1)).start_of_line();
            }

            let chars: Vec<char> = line.chars().collect();
            self.insert_chars_at(inspos, &chars);

            inspos = inspos.moved(Movement::right(chars.len() as i64));
        }
    }

    fn delete_at(&self, pos: Pos, mov: Movement)
    {
        self.delete_between(pos, pos.moved(mov).correct_wrap(self));
    }

    fn break_line(&self, cur: &Cursor<Self>)
    {
        self.break_line_at(cur.get_pos());
    }

    fn insert_chars(&self, cur: &Cursor<Self>, text: &[char])
    {
        self.insert_chars_at(cur.get_pos(), text);
    }

    fn delete(&self, cur: &Cursor<Self>, mov: Movement)
    {
        self.delete_at(cur.get_pos(), mov);
    }

    fn delete_backward(&self, cur: &Cursor<Self>, n: i64)
    {
        self.delete(cur, Movement::left(n))
    }

    fn delete_forward(&self, cur: &Cursor<Self>, n: i64)
    {
        self.delete(cur, Movement::right(n))
    }

    fn insert(&self, cur: &Cursor<Self>, text: &str)
    {
        self.insert_at(cur.get_pos(), text);
    }
}
