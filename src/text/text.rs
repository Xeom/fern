use std::iter;
use std::convert::TryFrom;
use super::{ToRow, Row, Col, Pos, Line};

pub trait Text
{
    fn num_lines(&self) -> usize;
    fn get_line_by_ind(&self, ind: usize) -> Option<Line>;

    fn get_line(&self, pos: &dyn ToRow) -> Option<Line>
    {
        let row = usize::try_from(pos.to_row()).ok()?;

        return self.get_line_by_ind(row);
    }

    fn line_len(&self, pos: &dyn ToRow) -> usize {
        self.get_line(pos).map(|l|l.len()).unwrap_or(0)
    }

    fn last_row(&self) -> Row
    {
        if self.num_lines() > 0
        {
            if let Ok(row) = Row::try_from(self.num_lines() - 1)
            {
                return row;
            }
        }

        return Row::zero();
    }

    fn last_pos(&self) -> Pos
    {
        let row = self.last_row();

        if let Ok(col) = Col::try_from(self.line_len(&row))
        {
            return Pos::new(row, col);
        }

        return Pos::zero();
    }
}

pub trait MutText: Text
{
    fn replace_lines_by_ind<L>(&self, ind: usize, n: usize, lines: L)
        where L: IntoIterator<Item=Line>;

    fn replace_lines<L>(&self, row: &dyn ToRow, n: usize, lines: L)
        where L: IntoIterator<Item=Line>
    {
        if let Ok(ind) = usize::try_from(row.to_row())
        {
            if ind <= self.num_lines()
            {
                self.replace_lines_by_ind(ind, n, lines);
            }
        }
    }

    fn insert_lines<L>(&self, pos: &dyn ToRow, lines: L)
        where L: IntoIterator<Item=Line>
    {
        self.replace_lines(pos, 0, lines);
    }

    fn remove_lines(&self, pos: &dyn ToRow, n: usize)
    {
        self.replace_lines(pos, n, iter::empty());
    }

    fn insert_line(&self, pos: &dyn ToRow, line: Line)
    {
        let lines = vec![line];
        self.insert_lines(pos, lines.into_iter());
    }

    fn insert_new_lines(&self, pos: &dyn ToRow, n: usize)
    {
        self.insert_lines(pos, iter::repeat_with(Line::new).take(n));
    }

    fn remove_line(&self, pos: &dyn ToRow) -> Option<Line>
    {
        let rtn = self.get_line(pos);
        self.remove_lines(pos, 1);

        return rtn;
    }

    fn append_line(&self, line: Line)
    {
        if let Ok(row) = Row::try_from(self.num_lines())
        {
            self.insert_line(&row, line);
        }
    }
}

