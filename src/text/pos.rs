use std::cmp;
use std::convert::TryFrom;
use super::{Movable, Shiftable, InnerMovable, InnerShiftable};
use super::{Movement, Text, Row, Col, Shift, ToRow, ToCol};

fn bound<T>(lower: T, v: T, upper: T) -> T
    where T: Ord
{
    cmp::max(lower, cmp::min(upper, v))
}

#[derive(Copy, Clone, PartialEq, Eq, Ord, PartialOrd, Debug)]
pub struct Pos
{
    pub row: Row,
    pub col: Col
}

pub trait ToPos
{
    fn to_pos(&self) -> Pos;
}

impl ToPos for Pos { fn to_pos(&self) -> Pos { self.clone() } }
impl ToRow for Pos { fn to_row(&self) -> Row { self.row } }
impl ToCol for Pos { fn to_col(&self) -> Col { self.col } }

impl<T: ToPos> ToPos for &T { fn to_pos(&self) -> Pos { (*self).to_pos() } }

impl Pos
{
    pub fn new(row: impl ToRow, col: impl ToCol) -> Self {
        Self { row: row.to_row(), col: col.to_col() }
    }

    pub fn zero() -> Self {
        Self::new(Row::zero(), Col::zero())
    }

    pub fn start_of_line(&self) -> Pos {
        Self::new(self.row, Col::zero())
    }

    pub fn end_of_line(&self, text: &dyn Text) -> Self
    {
        if let Ok(col) = Col::try_from(text.line_len(self))
        {
            return Self::new(self.row, col);
        }

        return self.clone();
    }

    pub fn correct_wrap(&mut self, text: &dyn Text) -> Self
    {
        let mut rtn = self.clone();

        rtn = bound(Pos::zero(), rtn, text.last_pos());

        while rtn > Pos::zero() && rtn < rtn.start_of_line()
        {
            rtn.do_move(Movement::up(1));
            rtn.do_move(Movement::right((text.line_len(&rtn) + 1) as i64));
        }

        while rtn < text.last_pos() && rtn > rtn.end_of_line(text)
        {
            rtn.do_move(Movement::left((text.line_len(&rtn) + 1) as i64));
            rtn.do_move(Movement::down(1));
        }

        rtn = bound(Pos::zero(), rtn, text.last_pos());

        return rtn;
    }

    pub fn correct_nowrap(&mut self, text: &dyn Text) -> Pos
    {
        let mut rtn = self.clone();

        rtn = bound(Pos::zero(), rtn, text.last_pos());
        rtn = bound(rtn.start_of_line(), rtn, rtn.end_of_line(text));

        return rtn;
    }
}

impl Shiftable for Pos
{
    fn shifted(&self, shift: Shift) -> Pos
    {
        if self < &shift.from && self > &shift.to
        {
            /* Inside the movement, when it's a deletion, we snap *
             * to the front.                                      */
            return shift.to;
        }
        else if self >= &shift.from
        {
            if self.row == shift.from.row
            {
                /* After the 'from', on the same line, we do the entire *
                 * shift as a movement.                                 */
                return self.moved(shift.to_movement());
            }
            else
            {
                /* Only do the y component of the movement. */
                return self.moved(Movement::down(shift.to_movement().y));
            }
        }
        else
        {
            return self.clone();
        }
    }
}

impl Movable for Pos
{
    fn moved(&self, mov: Movement) -> Self {
        Pos::new(
            self.row.moved(mov),
            self.col.moved(mov)
        )
    }
}

impl InnerShiftable for &mut Pos
{
    fn do_shift(self, shift: Shift)
    {
        *self = self.shifted(shift);
    }
}

impl InnerMovable for &mut Pos
{
    fn do_move(self, mov: Movement)
    {
        *self = self.moved(mov);
    }
}

