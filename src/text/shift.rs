use super::{Movement, Pos, Movable};

#[derive(Copy, Clone, Debug)]
pub struct Shift
{
    pub from: Pos,
    pub to:   Pos
}

pub trait Shiftable
{
    fn shifted(&self, shift: Shift) -> Self;
}

pub trait InnerShiftable
{
    fn do_shift(self, shift: Shift);
}

impl Shift
{
    pub fn new(from: Pos, to: Pos) -> Self {
        Self { from: from, to: to }
    }

    pub fn from_movement(pos: Pos, mov: Movement) -> Self {
        Self { from: pos, to: pos.moved(mov) }
    }

    pub fn to_movement(&self) -> Movement {
        Movement {
            x: i64::from(self.to.col) - i64::from(self.from.col),
            y: i64::from(self.to.row) - i64::from(self.from.row)
        }
    }
}

