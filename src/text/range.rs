struct Range
{
    a: Cursor,
    b: Cursor
};

impl Range
{
    pub fn new(text: &dyn CursorText, a: Pos, b: Pos) -> Self {
        Self { a: text.add_cursor_at(a), b: text.add_cursor_at(b) }
    }

    pub fn start(&self) -> Pos {
        cmp::min(self.a, self.b)
    }

    pub fn start_mut(&mut self) -> &mut Pos {
        cmp::min(&mut self.b, &mut self.a)
    }

    pub fn end(&self) -> Pos {
        cmp::max(self.a, self.b)
    }

    pub fn end_mut(&mut self) -> &mut Pos {
        cmp::max(&mut self.b, &mut self.a)
    }
}

trait HasRange
{
    fn to_range(&self) -> Range;
}
