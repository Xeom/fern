use super::{Movement};

/* Traits to implement */
use std::convert::TryFrom;
use super::{Movable, InnerMovable};

#[derive(Copy, Clone, PartialEq, Eq, Ord, PartialOrd, Debug)]
pub struct Row(i64);

#[derive(Copy, Clone, PartialEq, Eq, Ord, PartialOrd, Debug)]
pub struct Col(i64);

impl Col
{
    pub fn zero() -> Self { Self(0) }
}

impl Row
{
    pub fn zero() -> Self { Self(0) }
}

impl From<i64> for Row
{
    fn from(v: i64) -> Row { Row(v) }
}

impl From<i64> for Col
{
    fn from(v: i64) -> Col { Col(v) }
}

impl TryFrom<usize> for Row
{
    type Error = <i64 as TryFrom<usize>>::Error;

    fn try_from(v: usize) -> Result<Row, Self::Error> {
        Ok(Row(i64::try_from(v)?))
    }
}

impl TryFrom<usize> for Col
{
    type Error = <i64 as TryFrom<usize>>::Error;

    fn try_from(v: usize) -> Result<Col, Self::Error> {
        Ok(Col(i64::try_from(v)?))
    }
}

impl From<Row> for i64
{
    fn from(v: Row) -> i64 { v.0 }
}

impl From<Col> for i64
{
    fn from(v: Col) -> i64 { v.0 }
}

impl TryFrom<Col> for usize
{
    type Error = <usize as TryFrom<i64>>::Error;
    fn try_from(v: Col) -> Result<usize, Self::Error> {
        Ok(usize::try_from(v.0)?)
    }
}

impl TryFrom<Row> for usize
{
    type Error = <usize as TryFrom<i64>>::Error;
    fn try_from(v: Row) -> Result<usize, Self::Error> {
        Ok(usize::try_from(v.0)?)
    }
}

impl Movable for Col
{
    fn moved(&self, mov: Movement) -> Self {
        Col::from(i64::from(*self) + mov.x)
    }
}

impl Movable for Row
{
    fn moved(&self, mov: Movement) -> Self {
        Row::from(i64::from(*self) + mov.y)
    }
}

impl InnerMovable for &mut Row
{
    fn do_move(self, mov: Movement) { *self = self.moved(mov); }
}

impl InnerMovable for &mut Col
{
    fn do_move(self, mov: Movement) { *self = self.moved(mov); }
}

pub trait ToRow
{
    fn to_row(&self) -> Row;
}

pub trait ToCol
{
    fn to_col(&self) -> Col;
}

impl ToRow for Row { fn to_row(&self) -> Row { *self } }
impl ToCol for Col { fn to_col(&self) -> Col { *self } }

impl<T: ToCol> ToCol for &T { fn to_col(&self) -> Col { (*self).to_col() } }
impl<T: ToRow> ToRow for &T { fn to_row(&self) -> Row { (*self).to_row() } }

