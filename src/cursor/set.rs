use crate::text::{Shift, InnerShiftable};
use std::cell::{RefCell};
use std::rc::{Rc, Weak};
use weak_table::PtrWeakHashSet;

use super::CursorPos;

pub struct CursorSet
{
    curs: RefCell<PtrWeakHashSet<Weak<CursorPos>>>
}

impl CursorSet
{
    pub fn new() -> Self { Self {curs: RefCell::new(PtrWeakHashSet::new()) } }

    pub fn add_cursor_pos(&self) -> Rc<CursorPos>
    {
        let curpos = Rc::new(CursorPos::new());
        self.curs.borrow_mut().insert(curpos.clone());

        return curpos;
    }
}

impl InnerShiftable for &CursorSet
{
    fn do_shift(self, shift: Shift)
    {
        for cur in self.curs.borrow().iter()
        {
            cur.do_shift(shift);
        }
    }
}
