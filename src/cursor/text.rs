use crate::text::{Pos, Shift, Text, InnerShiftable};
use std::rc::{Rc};
use super::{CursorPos, Cursor, CursorSet};

pub trait CursorText: Text + Sized
{
    fn get_cursors(&self) -> &CursorSet;

    fn shift_all(&self, shift: Shift) {
        self.get_cursors().do_shift(shift)
    }

    fn add_cursor_pos(&self) -> Rc<CursorPos> {
        self.get_cursors().add_cursor_pos()
    }

    fn add_cursor<'a>(&'a self) -> Cursor<'a, Self> {
        Cursor::new(self)
    }

    fn add_cursor_at<'a>(&'a self, pos: Pos) -> Cursor<'a, Self>
    {
        let cur = self.add_cursor();
        cur.set_pos(pos);

        return cur;
    }
}

