use crate::text::{Pos, Movement, Row, Col};
use super::{CursorText, CursorPos};
use std::rc::Rc;

/* Traits to implement. */
use crate::text::{InnerMovable, ToRow, ToCol, ToPos};

#[derive(Debug, Clone)]
pub struct Cursor<'a, T>
    where T: CursorText
{
    text: &'a T,
    pos: Rc<CursorPos>
}

impl<'a, T> Cursor<'a, T>
    where T: CursorText
{
    pub fn new(text: &'a T) -> Self {
        Self { text: text, pos: text.add_cursor_pos() }
    }

    pub fn get_pos(&self) -> Pos { self.pos.get_pos() }
    pub fn set_pos(&self, pos: Pos) { self.pos.set_pos(pos) }

    pub fn get_text(&self) -> &T { self.text }
}

impl<T> ToRow for Cursor<'_, T>
    where T: CursorText
{
    fn to_row(&self) -> Row { self.get_pos().to_row() }
}

impl<T> ToCol for Cursor<'_, T>
    where T: CursorText
{
    fn to_col(&self) -> Col { self.get_pos().to_col() }
}

impl<T> ToPos for Cursor<'_, T>
    where T: CursorText
{
    fn to_pos(&self) -> Pos { self.get_pos() }
}

impl<T> InnerMovable for &Cursor<'_, T>
    where T: CursorText
{
    fn do_move(self, mov: Movement)
    {
        let mut pos = self.get_pos();
        pos.do_move(mov);

        if mov.y == 0
        {
            pos = pos.correct_wrap(self.get_text());
        }
        else
        {
            pos = pos.correct_nowrap(self.get_text());
        }

        self.set_pos(pos);
    }
}
