use crate::text::{Pos, Shift};
use std::cell::{RefMut, RefCell};

use crate::text::InnerShiftable;

#[derive(Debug)]
pub struct CursorPos
{
    pos: RefCell<Pos>
}

impl CursorPos
{
    pub fn new() -> Self {
        Self { pos: RefCell::new(Pos::zero()) }
    }

    pub fn set_pos(&self, pos: Pos)
    {
        self.pos.replace(pos);
    }

    pub fn get_pos(&self) -> Pos {
        self.pos.borrow().clone()
    }

    pub fn get_pos_mut(&self) -> RefMut<'_, Pos> {
        self.pos.borrow_mut()
    }
}

impl InnerShiftable for &CursorPos
{
    fn do_shift(self, shift: Shift)
    {
        self.get_pos_mut().do_shift(shift);
    }
}

