pub mod text;
pub mod display;
pub mod buffer;
pub mod file;
pub mod err;
pub mod cursor;
pub use err::{FernError, FernResult, ErrorType, IntoFernError};
pub use text::{Movable, InnerMovable, Shiftable, InnerShiftable};
