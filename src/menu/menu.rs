trait MenuItem
{
    pub fn to_line(&self) -> Line {
        Line::from_string("Beans")
    }
}

struct Menu
{
    items: RefCell<Vec<Box<MenuItem>>>,
    curs:  RefCell<CursorSet>
}

impl Menu
{
    fn new() -> Self {
        Self {
            items: RefCell::new(Vec::new),
            curs:  RefCell::new(CursorSet::new())
        }
    }

    fn add_item(&mut self, item: impl MenuItem)
    {
        self.items.borrow().push(Box::new(item));
    }
}

impl Text for Menu
{
    fn num_lines(&self) -> i64 {
        self.items.borrow().len()
    }

    fn get_line(&self, pos: &dyn ToRow) -> Option<Line>
    {
        let row = pos.to_row();
        return self.items.borrow().get(usize::from(row)).to_line();
    }
}

impl CursorText for Buffer
{
    fn shift_all(&self, shift: Shift) {
        self.curs.borrow_mut().shift_all(shift)
    }

    fn add_cursor_pos(&self) -> Rc<CursorPos> {
        self.curs.borrow_mut().add_cursor_pos()
    }
}

