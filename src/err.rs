use std::fmt;
use std::error;

#[derive(Copy, Clone, Debug)]
pub enum ErrorType
{
    IOError,
    EncodingError,
    FmtError,
    IntError
}

pub type FernResult<T> = Result<T, FernError>;

#[derive(Debug)]
pub struct FernError
{
    typ: ErrorType,
    msg: String
}


impl FernError
{
    pub fn new(typ: ErrorType, msg: impl Into<String>) -> Self {
        Self {
            typ: typ,
            msg: format!("{:?} - {}", typ, msg.into())
        }
    }
}

impl fmt::Display for FernError
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl error::Error for FernError
{
    fn description(&self) -> &str { &self.msg }
}

impl<E: IntoFernError> From<E> for FernError
{
    fn from(err: E) -> Self {
        Self::new(E::TYPE, format!("{}", err))
    }
}

pub trait IntoFernError: error::Error
{
    const TYPE: ErrorType;
}

impl IntoFernError for std::io::Error
{
    const TYPE: ErrorType = ErrorType::IOError;
}

impl IntoFernError for std::str::Utf8Error
{
    const TYPE: ErrorType = ErrorType::EncodingError;
}

impl IntoFernError for std::fmt::Error
{
    const TYPE: ErrorType = ErrorType::FmtError;
}

impl IntoFernError for std::num::TryFromIntError
{
    const TYPE: ErrorType = ErrorType::IntError;
}
