use std::io;
use crate::{FernResult};

const BUFSIZE: usize = 256;

pub struct Loader<S, D>
    where S: io::Read, D: io::Write
{
    src: S,
    dst: D
}

#[derive(Eq, PartialEq)]
pub enum LoadState
{
    Done,
    Loading
}

impl<S, D> Loader<S, D>
    where S: io::Read, D: io::Write
{
    pub fn new(src: S, dst: D) -> Self {
        Self { src: src, dst: dst }
    }

    pub fn load_all(&mut self) -> FernResult<()>
    {
        while LoadState::Loading == self.load()? { }

        return Ok(());
    }

    pub fn load(&mut self) -> FernResult<LoadState>
    {
        let mut buf: [u8; BUFSIZE] = [0; BUFSIZE];
        let nbytes  = self.src.read(&mut buf)?;
        let mut off = 0;

        while off < nbytes
        {
            off += self.dst.write(&buf[off..nbytes])?;
        }

        if nbytes == 0
        {
            return Ok(LoadState::Done);
        }
        else
        {
            return Ok(LoadState::Loading);
        }

    }
}

