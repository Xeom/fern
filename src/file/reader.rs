use std::io;
use std::cmp::min;
use std::collections::VecDeque;
use crate::cursor::{Cursor, CursorText};
use crate::text::{Movement, Line, Pos, InnerMovable};

pub struct Reader<'a, T>
    where T: CursorText
{
    text: &'a T,
    cur:  Cursor<'a, T>,
    buf:  VecDeque<u8>
}

impl<'a, T> Reader<'a, T>
    where T: CursorText
{
    pub fn new(text: &'a T) -> Self {
        Self { text: text, cur: Cursor::new(text), buf: VecDeque::new() }
    }

    fn at_end_of_text(&mut self) -> bool {
        self.cur.get_pos() >= self.text.last_pos()
    }

    pub fn at_eof(&mut self) -> bool {
        self.at_end_of_text() && self.buf.len() == 0
    }

    pub fn set_pos(&mut self, pos: Pos) {
        self.cur.set_pos(pos)
    }

    fn seek(&mut self, n: i64) {
        self.cur.do_move(Movement::right(n))
    }

    fn encode_char(&mut self, ch: char)
    {
        let mut tmp: [u8; 8] = [0; 8];

        for byte in ch.encode_utf8(&mut tmp).as_bytes()
        {
            self.buf.push_back(*byte);
        }
    }

    fn encode_line(&mut self, line: &Line)
    {
        for ch in line.chars()
        {
            self.encode_char(ch);
        }

        self.encode_char('\n');
    }

    fn fill_buffer(&mut self)
    {
        if let Some(line) = self.text.get_line(&self.cur)
        {
            self.encode_line(&line);
            self.seek((1 + line.len()) as i64);
        }
    }
}

impl<T> io::Read for Reader<'_, T>
    where T: CursorText
{
    fn read(&mut self, dst: &mut [u8]) -> io::Result<usize>
    {
        while self.buf.len() < dst.len() && !self.at_end_of_text()
        {
            self.fill_buffer();
        }

        let n = min(dst.len(), self.buf.len());

        for ind in 0..n
        {
            dst[ind] = self.buf.pop_front().unwrap_or(0x00);
        }

        return Ok(n);
    }
}
