use std::io;
use std::char;
use std::str;
use crate::cursor::Cursor;
use crate::text::{EditableText};

pub struct Decoder
{
    buf: [u8; 4],
    len: usize
}

impl Decoder
{
    pub fn new() -> Self {
        Self { buf: [0; 4], len: 0 }
    }

    pub fn byte(&mut self, byte: u8, rtn: &mut String)
    {
        if self.len < self.buf.len()
        {
            self.buf[self.len] = byte;
            self.len += 1;
        }

        match str::from_utf8(&self.buf[..self.len])
        {
            Ok(s) =>
            {
                rtn.push_str(s);
                self.len = 0;
            }
            Err(utferr) =>
            {
                if utferr.error_len() != None
                {
                    self.len = 0;
                    rtn.push(char::REPLACEMENT_CHARACTER);
                }
            }
        }
    }
}

pub struct Writer<'a, T>
    where T: EditableText
{
    text:    &'a T,
    cur:     Cursor<'a, T>,
    decoder: Decoder
}

impl<'a, T> Writer<'a, T>
    where T: EditableText
{
    pub fn new(text: &'a T) -> Self {
        Self {
            text: text,
            cur: Cursor::new(text),
            decoder: Decoder::new()
        }
    }

    fn write_string(&mut self, s: &str)
    {
        self.text.insert(&self.cur, s);
    }
}

impl<T> io::Write for Writer<'_, T>
    where T: EditableText
{
    fn write(&mut self, buf: &[u8]) -> io::Result<usize>
    {
        let mut string = String::new();

        for byte in buf
        {
            self.decoder.byte(*byte, &mut string)
        }

        self.write_string(&string);

        return Ok(buf.len());
    }

    fn flush(&mut self) -> io::Result<()> { Ok(()) }
}
