use fern::display::{Display, Window};
use fern::buffer::Buffer;
use fern::cursor::Cursor;
use fern::text::{EditableText, Movement, InnerMovable};
use fern::file::{Loader, Writer, Reader};
use fern::FernResult;

use std::io;
use std::fs;

use termion::event::Key;
use termion::input::TermRead;

fn main() -> FernResult<()> {
    let stdin  = io::stdin();

    let mut display = Display::from_stdout()?;
    let     buf     = Buffer::new();

    Loader::new(
        fs::File::open("example.txt")?,
        Writer::new(&buf)
    ).load_all()?;

    let cur  = Cursor::new(&buf);
    let win  = Window::new(display.width(), display.height());

    display.set_cursor_pos(cur.get_pos());
    display.print(win.display_text(&buf))?;

    for k in stdin.keys()
    {
        match k.unwrap()
        {
            Key::Char('x') => break,
            Key::Char('\n')=> buf.break_line(&cur),
            Key::Left      => cur.do_move(Movement::left(1)),
            Key::Right     => cur.do_move(Movement::right(1)),
            Key::Up        => cur.do_move(Movement::up(1)),
            Key::Down      => cur.do_move(Movement::down(1)),
            Key::Char(c)   => buf.insert(&cur, &String::from(c)),
            Key::Backspace => buf.delete_backward(&cur, 1),
            Key::Delete    => buf.delete_forward(&cur, 1),
            _              => {}
        }
        display.set_cursor_pos(cur.get_pos());
        display.print(win.display_text(&buf))?;
    }

    Loader::new(
        Reader::new(&buf),
        fs::File::create("example-out.txt")?
    ).load_all()?;

    return Ok(());
}
