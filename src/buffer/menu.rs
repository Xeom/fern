

pub struct Menu
{
    lines: RefCell<Vec<Line>>,
    links: RefCell<Vec<Link>>,
    curs:  CursorSet
}

impl Text for Menu
{
    fn num_lines(&self) -> usize { self.lines.borrow().len() }

    fn get_line_by_ind(&self, ind: usize) -> Option<Line> {
        self.lines.borrow().get(ind)
    }
}

impl CursorText for Menu
{
    fn get_cursors(&self) -> &CursorSet { self.curs }
}



