use std::cell::RefCell;
use crate::cursor::{CursorText, CursorSet};
use crate::text::{Line, Text, MutText, EditableText};

pub struct Buffer
{
    lines: RefCell<Vec<Line>>,
    curs:  CursorSet
}

impl Buffer
{
    pub fn new() -> Self {
        Self {
            lines: RefCell::new(vec![Line::new()]),
            curs:  CursorSet::new()
        }
    }
}

impl Text for Buffer
{
    fn num_lines(&self) -> usize { self.lines.borrow().len() }

    fn get_line_by_ind(&self, ind: usize) -> Option<Line> {
        self.lines.borrow().get(ind).cloned()
    }
}

impl MutText for Buffer
{
    fn replace_lines_by_ind<L>(&self, ind: usize, n: usize, lines: L)
        where L: IntoIterator<Item=Line>
    {
        self.lines.borrow_mut().splice(
            ind..(ind + n),
            lines.into_iter()
        );
    }
}

impl CursorText for Buffer
{
    fn get_cursors(&self) -> &CursorSet { &self.curs }
}

impl EditableText for Buffer
{
}
