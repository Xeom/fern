use std::fmt;
use std::rc::Rc;
use std::sync;
use termion::{color, style};

#[derive(Debug, PartialEq, Eq)]
struct FontInfo { seq: String }

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Font { info: Rc<FontInfo> }

impl FontInfo
{
    const fn new() -> Self { Self { seq: String::new() } }

    fn append(&self, disp: &dyn fmt::Display) -> Self {
        Self { seq: format!("{}{}", self.seq, disp) }
    }
}

impl Font
{
    fn from_info(info: FontInfo) -> Self {
        Self { info: Rc::new(info) }
    }

    pub fn new() -> Self {
        static mut DEFAULT: Option<Font> = None;
        static ONCE:        sync::Once   = sync::Once::new();

        return unsafe {
            ONCE.call_once(
                || { DEFAULT = Some(Self::from_info(FontInfo::new())); }
            );
            DEFAULT.clone().unwrap()
        }
    }

    pub fn with(&self, disp: &dyn fmt::Display) -> Self {
        Self::from_info(self.info.append(disp))
    }

    pub fn with_fg(&self, col: &dyn color::Color) -> Self {
        self.with(&color::Fg(col))
    }

    pub fn with_bg(self, col: &dyn color::Color) -> Self {
        self.with(&color::Bg(col))
    }
}

impl fmt::Display for Font
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{reset}{colour}", reset=style::Reset, colour=self.info.seq)
    }
}
