use crate::text::{Pos, Text};
use super::{DispLine, Walker, Font};

pub struct Window
{
    #[allow(dead_code)]
    width:  usize,
    #[allow(dead_code)]
    height: usize,
    scroll: Pos,
}

struct LineCounter<T>
    where T: Iterator<Item=DispLine>
{
    prev:  i64,
    width: usize,
    lines: T
}

impl<T> LineCounter<T>
    where T: Iterator<Item=DispLine>
{
    pub fn new(width: usize, lines: T) -> Self {
        Self { prev: -1, width: width, lines: lines }
    }

    pub fn fit_num(&self, mut s: String) -> String
    {
        if s.ends_with("000")
        {
            let stripped = s.trim_end_matches('0');
            let base     = s.len() - stripped.len();

            s = format!("{strip}^{base}", strip=stripped, base=base);
        }

        if s.len() > self.width
        {
            s = s.split_off(s.len() - self.width);
        }

        return s;
    }

    pub fn format(&mut self, v: i64) -> String
    {
        if self.prev == v
        {
            return format!("{:.<width$} ", "", width=self.width);
        }
        else
        {
            let lineno = self.fit_num(v.to_string());
            return format!("{row:>width$}>", row=lineno, width=self.width);
        }
    }

    pub fn add_lineno(&mut self, mut line: DispLine) -> DispLine
    {
        let row    = line.first_pos().row;
        let prefix = self.format(i64::from(row));
        self.prev  = i64::from(row);

        line.prepend_str(&prefix, &Font::new());

        return line;
    }
}

impl<T> Iterator for LineCounter<T>
    where T: Iterator<Item=DispLine>
{
    type Item = DispLine;

    fn next(&mut self) -> Option<DispLine> {
        self.lines.next().map(|l| self.add_lineno(l))
    }
}

impl Window
{
    pub fn new(width: usize, height: usize) -> Self {
        Self { width: width, height: height, scroll: Pos::zero() }
    }

    pub fn display_text<'a>(&self, text: &'a impl Text)
        -> impl Iterator<Item=DispLine> + 'a
    {
        let walker  = Walker::new(text, &self.scroll, self.width as usize - 5);
        let counter = LineCounter::new(3, walker);

        return counter.take(self.height);
    }
}
