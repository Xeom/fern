use std::io;
use std::fmt::Write;
use std::collections::VecDeque;

use termion::cursor;

use crate::FernResult;
use crate::text::{Movement, Pos, Movable};

use super::{Display, Font};

#[derive(Clone, Debug)]
pub struct DispChar
{
    c:    char,
    font: Font,
    pos:  Option<Pos>
}

impl DispChar
{
    pub fn new(c: char) -> Self {
        Self { c: c, font: Font::new(), pos: None }
    }

    pub fn set_pos(&mut self, pos: Pos)
    {
        self.pos = Some(pos);
    }

    pub fn set_font(&mut self, font: &Font) { self.font = font.clone(); }
    pub fn get_font(&self) -> Font { self.font.clone() }

    pub fn print<T>(&self, disp: &mut Display<T>) -> FernResult<()>
        where T: io::Write
    {
        disp.set_font(self.font.clone())?;

        if let Some(pos) = self.pos
        {
            disp.handle_pos(pos)?;
        }

        write!(disp, "{c}", c=self.c)?;

        return Ok(());
    }
}

pub struct DispLine
{
    start: Pos,
    chars: VecDeque<DispChar>
}

impl DispLine
{
    pub fn new(start: Pos) -> Self {
        Self { start: start, chars: VecDeque::new() }
    }

    pub fn len(&self) -> usize {
        self.chars.len()
    }

    pub fn push_back(&mut self, dc: DispChar)
    {
        self.chars.push_back(dc);
    }

    pub fn push_front(&mut self, dc: DispChar)
    {
        self.chars.push_front(dc);
    }

    pub fn append_str(&mut self, s: &str, font: &Font)
    {
        for c in s.chars()
        {
            let mut dc = DispChar::new(c);
            dc.set_font(&font);
            self.push_back(dc);
        }
    }

    pub fn prepend_str(&mut self, s: &str, font: &Font)
    {
        for c in s.chars().rev()
        {
            let mut dc = DispChar::new(c);
            dc.set_font(&font);
            self.push_front(dc);
        }
    }

    pub fn first_pos(&self) -> Pos {
        self.start
    }

    pub fn last_pos(&self) -> Pos
    {
        for dc in self.chars.iter().rev()
        {
            if let Some(pos) = dc.pos
            {
                return pos.moved(Movement::right(1));
            }
        }

        return self.start;
    }

    pub fn print<T>(&self, disp: &mut Display<T>) -> FernResult<()>
        where T: io::Write
    {
        write!(disp, "{save}", save=cursor::Save)?;

        for dc in self.chars.iter()
        {
            dc.print(disp)?;
        }

        disp.set_font(Font::new())?;
        disp.handle_pos_eol(self.last_pos())?;
        disp.next_line()?;

        return Ok(());
    }
}

//impl std::ops::Deref for DispLine
//{
//    type Target = Vec<DispChar>;
//    fn deref(&self) -> &Self::Target { &self.chars }
//}

//impl std::ops::DerefMut for DispLine
//{
//    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.chars }
//}

