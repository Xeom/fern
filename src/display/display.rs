use crate::text::Pos;
use termion::raw::IntoRawMode;
use std::io;
use std::convert::TryFrom;
use std::fmt;
use std::fmt::Write;
use termion::{screen, clear, cursor, raw};
use crate::{FernResult};
use super::{DispLine, Font};

pub struct Display<T>
    where T: io::Write
{
    #[allow(dead_code)]
    width:  u16,
    #[allow(dead_code)]
    height: u16,

    term:   T,

    curpos:     Pos,
    defaultpos: (u16, u16),
    font:       Font
}

impl Display<screen::AlternateScreen<raw::RawTerminal<io::Stdout>>>
{
    pub fn from_stdout() -> FernResult<Self>
    {
        let stdout  = io::stdout();
        let raw     = stdout.into_raw_mode()?;
        let screen  = screen::AlternateScreen::from(raw);
        let mut rtn = Display::new(screen);

        rtn.set_dims(termion::terminal_size()?);

        return Ok(rtn);
    }
}

impl<T> Display<T>
    where T: io::Write
{
    pub fn new(term: T) -> Self {
        Self {
            width:      80,
            height:     30,
            term:       term,
            curpos:     Pos::zero(),
            defaultpos: (1, 1),
            font:       Font::new()
        }
    }

    pub fn width(&self) -> usize {
        self.width as usize
    }

    pub fn height(&self) -> usize {
        self.height as usize
    }


    pub fn set_dims(&mut self, dims: (u16, u16))
    {
        self.width  = dims.0;
        self.height = dims.1;
    }

    pub fn set_font(&mut self, newfont: Font) -> FernResult<()>
    {
        if self.font != newfont
        {
            write!(self, "{font}", font=newfont)?;
            self.font = newfont;
        }

        return Ok(());
     }

    pub fn next_line(&mut self) -> FernResult<()>
    {
        write!(
            self,
            "{clear}{restore}",
            clear=clear::UntilNewline, restore=cursor::Restore
        )?;

        return Ok(());
    }

    fn detect_cur_pos(&mut self, curpos: Pos, termpos: Pos)
        -> FernResult<()>
    {
        let detector = &mut self.term as &mut dyn cursor::DetectCursorPos;
        let here     = detector.cursor_pos()?;
        let off      = i64::from(curpos.col) - i64::from(termpos.col);

        self.defaultpos = (
            here.0,
            here.1 + u16::try_from(off)?
        );

        return Ok(());
    }

    pub fn handle_pos_eol(&mut self, termpos: Pos) -> FernResult<()>
    {
        if self.curpos.row == termpos.row && self.curpos.col >= termpos.col
        {
            self.detect_cur_pos(self.curpos, termpos)?;
        }

        return Ok(());
    }


    pub fn handle_pos(&mut self, termpos: Pos) -> FernResult<()>
    {
        if self.curpos == termpos
        {
            self.detect_cur_pos(self.curpos, termpos)?;
        }

        return Ok(());
    }

    pub fn set_cursor_pos(&mut self, pos: Pos)
    {
        self.curpos = pos;
    }

    pub fn print(&mut self, text: impl Iterator<Item=DispLine>)
        -> FernResult<()>
    {
        write!(
            self, "{hide}{goto}{clear}",
            clear=clear::All, hide=cursor::Hide, goto=cursor::Goto(1, 1)
        )?;

        for line in text
        {
            line.print(self)?;
            write!(self, "{down}", down=cursor::Down(1))?;
        }

        write!(
            self, "{goto}{show}",
            show=cursor::Show, goto=cursor::Goto(
                self.defaultpos.0, self.defaultpos.1
            )
        )?;

        self.term.flush()?;

        return Ok(());
    }
}

impl<T> fmt::Write for Display<T>
    where T: io::Write
{
    fn write_str(&mut self, s: &str) -> fmt::Result {
        match write!(self.term, "{s}", s=s) {
            Ok(_)  => Ok(()),
            Err(_) => Err(fmt::Error)
        }
    }
}

