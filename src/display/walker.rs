use crate::text::{Pos, Text, Movement, ToRow, Col, InnerMovable};
use super::{DispChar, DispLine};

pub struct Walker<'a>
{
    pos:   Pos,
    width: usize,
    text:  &'a dyn Text
}

impl<'a> Walker<'a>
{
    pub fn new(text: &'a dyn Text, row: &dyn ToRow, width: usize) -> Self {
        Self {
            pos: Pos::new(row.to_row(), Col::zero()),
            text: text,
            width: width
        }
    }
}

impl Iterator for Walker<'_>
{
    type Item = DispLine;

    fn next(&mut self) -> Option<DispLine>
    {
        let line = self.text.get_line(&self.pos)?;
        let mut rtn = DispLine::new(self.pos);

        for c in line.after(&self.pos).chars()
        {
            let mut dc = DispChar::new(c);
            dc.set_pos(self.pos);

            rtn.push_back(dc);
            self.pos.do_move(Movement::right(1));

            if rtn.len() >= self.width {
                return Some(rtn);
            }
        }

        self.pos.do_move(Movement::down(1));
        self.pos = self.pos.start_of_line();

        return Some(rtn);
    }
}
